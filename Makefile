PROJECT = crypto
MIX = mix
BUILD_DIR = _build

.PHONY: clean test all release

all: test release

release:
	MIX_ENV=prod $(MIX) release crypto --overwrite

app: deps test format_test
	$(MIX) compile --warnings-as-errors

deps:
	$(MIX) deps.get

clean:
	@$(MIX) clean
	@if [ -f erl_crash.dump ]; then rm erl_crash.dump; fi
	
test:
	MIX_ENV=test $(MIX) test

analyse:
	$(MIX) credo --strict

format:
	@$(MIX) format

format_test:
	$(MIX) format --check-formatted

coverage:
	MIX_ENV=test $(MIX) coveralls	

ssh:
	ssh $(HOST)

cp_willy:
ifndef VERSION
	$(error VERSION is undefined)
endif
	scp _build/prod/$(PROJECT)-$(VERSION).tar.gz $(HOST):~/projects/elixir/$(PROJECT).tar.gz

db_create:
	SECRET_KEY_BASE=RhjWjhJaIwXKSvffWEHWyaUcgld/6lkt17a5LFn3WU9ns215uQQ0gdAUHrk1BCOP DATABASE_URL=ecto://postgres:postgres@localhost/backend_dev $(MIX) ecto.create ecto.migrate

run_dev:
	 MIX_ENV=dev PORT=4050 HOST=localhost SECRET_KEY_BASE=RhjWjhJaIwXKSvffWEHWyaUcgld/6lkt17a5LFn3WU9ns215uQQ0gdAUHrk1BCOP DATABASE_URL=ecto://postgres:postgres@localhost/backend_dev iex --sname crypto-app --cookie secret -S mix phx.server

run_release:
	PORT=4050 SECRET_KEY_BASE=RhjWjhJaIwXKSvffWEHWyaUcgld/6lkt17a5LFn3WU9ns215uQQ0gdAUHrk1BCOP HOST=localhost DATABASE_URL=ecto://postgres:postgres@localhost/backend_dev _build/prod/rel/crypto/bin/crypto start_iex

deploy:
	docker image build --platform="linux/arm64" -t fiatbitcoin:latest .
	docker save -o fiatbitcoin.tar fiatbitcoin:latest
	scp -i $(HETZNER_CERT) fiatbitcoin.tar $(HETZNER_HOST):~/

ssh_fiat:
	ssh -i $(HETZNER_CERT) $(HETZNER_HOST)
