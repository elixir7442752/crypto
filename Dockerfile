ARG MIX_ENV="prod"

# build stage
FROM elixir:1.16.0-alpine

RUN apk update;  \
    apk add git; \
    apk add git; \
    apk add curl gnupg; \
    curl -sL https://deb.nodesource.com/setup_20.x  | bash -; \
    apk add nodejs; \
    apk add npm

RUN mkdir -p /crypto

# sets work dir
WORKDIR /crypto

# install hex + rebar
RUN mix local.hex --force && \
    mix local.rebar --force

# set build ENV
ARG MIX_ENV
ENV MIX_ENV="${MIX_ENV}"

#ENV SECRET_KEY_BASE="${SECRET_KEY_BASE}"

# install mix dependencies
COPY mix.exs mix.lock ./
COPY config config

# copy all umbrella apps under apps
COPY apps apps

# copy all scripts
COPY scripts scripts

# create the  upload directory
RUN mkdir -p apps/customer_web/priv/static/uploads/

# get all dependencies and compile them
RUN mix deps.get
RUN mix deps.compile

RUN cd apps/customer_web/assets
RUN npm i -D daisyui@latest
RUN npm i --save vega vega-embed vega-lite

RUN cd ../../..

# copy the release configuration
COPY rel rel

# compile and build the release
RUN mix compile

# create the assets
RUN mix assets.deploy

# create a release
RUN mix release

EXPOSE 80

ENTRYPOINT ["./scripts/start.sh"]

# Usage:
#  * build: docker image build --platform="linux/arm64" -t fiatbitcoin .
#  * shell: sudo docker container run --rm -it --entrypoint "" -p 127.0.0.1:4000:4000 elixir/crypto sh
#  * run:   sudo docker container run --rm -it -p 127.0.0.1:4050:4050 -e HOST=localhost -e SECRET_KEY_BASE=$SECRET_KEY_BASE -e DATABASE_URL=$DATABASE_URL --network crypto-network --name crypto elixir/crypto
#  * exec:  sudo docker container exec -it my_app sh
#  * logs:  sudo docker container logs --follow --tail 100 crypto
