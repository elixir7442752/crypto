#  Details

You can import your csv downloads from your [crypto.com](crypto.com) app and have a detail view over yout invests. 

# Requirements

You can see the required erlang runtime and elixir in the .envrc file. [direnvs](https://direnv.net)
# Getting started

Clone [crypto](git@gitlab.com:uangermann/crypto.git)

# Registry Authentication 
htpasswd -B <user> <password> > htpasswd_backup/htpasswd

