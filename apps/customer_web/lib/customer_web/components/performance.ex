defmodule CustomerWeb.Performance do
  use Phoenix.VerifiedRoutes, endpoint: CustomerWeb.Endpoint, router: CustomerWeb.Router
  use Phoenix.LiveComponent
  alias VegaLite
  require Logger

  @impl true
  def update(assigns, socket) do
    spec =
      VegaLite.new(width: :container, height: :container)
      |> VegaLite.data_from_values(prepare_data(assigns))
      |> VegaLite.mark(:area, color: "#4682b4", line: [color: "#F7931A"], tooltip: true)
      |> VegaLite.encode_field(:y, "value", type: :quantitative, title: "Value", axis: true)
      |> VegaLite.encode_field(:x, "date", type: :temporal, title: "Time", axis: true)
      |> VegaLite.to_spec()

    socket = assign(socket, id: assigns.id)

    {:ok, push_event(socket, "vega_lite:#{socket.assigns.id}:init", %{"spec" => spec})}
  end

  @impl true
  def render(assigns) do
    ~H"""
    <div
      style="width:100%; height: 100%"
      id={"vega-lite-#{@id}"}
      phx-hook="VegaLite"
      phx-update="ignore"
      data-id={@id}
    />
    """
  end

  def prepare_data(assigns) do
    assigns.graphdata
  end
end
