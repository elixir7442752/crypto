defmodule CustomerWeb.Router do
  use CustomerWeb, :router

  import CustomerWeb.UserAuth
  import LiveAdmin.Router


  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {CustomerWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  # Other scopes may use custom stacks.
  # scope "/api", CustomerWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test, :prod] do
    import Phoenix.LiveDashboard.Router

    scope "/admin" do
      pipe_through [:browser, :require_authenticated_user_and_admin]
      live_dashboard "/dashboard", metrics: CustomerWeb.Telemetry, ecto_repos: [Backend.Repo]
    end
  end

  if Mix.env() == :dev do
    # If using Phoenix
    forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end

  scope "/" do
    pipe_through [:browser, :require_authenticated_user_and_admin]

    live_admin "/admin" do
      admin_resource("/users", Backend.Accounts.User)
      admin_resource("/pocket", Backend.Data.PocketPurchase)
      admin_resource("/crypto", Backend.Data.CryptoPurchase)
      admin_resource("/card", Backend.Data.CardTransaction)
      admin_resource("/etoro", Backend.Data.EtoroGbt)
      admin_resource("/manual", Backend.Data.ManualPurchase)
      admin_resource("/viban", Backend.Data.VibanPurchase)
      admin_resource("/bitcoin", Backend.Data.BitcoinPrice)
    end
  end

  ## Authentication routes

  scope "/", CustomerWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create
    get "/users/reset_password", UserResetPasswordController, :new
    post "/users/reset_password", UserResetPasswordController, :create
    get "/users/reset_password/:token", UserResetPasswordController, :edit
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/", CustomerWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_session :default do
      live "/purchases/manual_purchases", ManualsLive, :index
      live "/purchases/manual_purchases/new", ManualsLive, :new
      live "/purchases/manual_purchases/:id", ManualsLive, :edit
      live "/purchases/:type", DetailsLive, :index
      live "/upload", Upload.UploadLive, :index
      live "/overview", OverviewLive, :index
      live "/overview/chart", OverviewLive, :chart
    end

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings", UserSettingsController, :update
    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email
    delete "/users/delete", UserRegistrationController, :delete
  end

  scope "/", CustomerWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :confirm
  end

  scope "/", CustomerWeb do
    pipe_through :browser
    live "/", OverviewLive, :index
  end

end
