defmodule CustomerWeb.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the PubSub system
      {Phoenix.PubSub, name: CustomerWeb.PubSub},
      # Start the Telemetry supervisor
      CustomerWeb.Telemetry,
      # Start the Endpoint (http/https)
      CustomerWeb.Endpoint
      # Start a worker by calling: CustomerWeb.Worker.start_link(arg)
      # {CustomerWeb.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: CustomerWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    CustomerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
