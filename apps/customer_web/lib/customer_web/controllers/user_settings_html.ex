defmodule CustomerWeb.UserSettingsHTML do
  use CustomerWeb, :html

  embed_templates "user_settings_html/*"
end
