defmodule CustomerWeb.UserRegistrationHTML do
  use CustomerWeb, :html

  embed_templates "user_registration_html/*"
end
