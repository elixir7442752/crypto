defmodule CustomerWeb.UserConfirmationHTML do
  use CustomerWeb, :html

  embed_templates "user_confirmation_html/*"
end
