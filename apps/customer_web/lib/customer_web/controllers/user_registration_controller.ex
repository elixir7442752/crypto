defmodule CustomerWeb.UserRegistrationController do
  use CustomerWeb, :controller

  alias Backend.Accounts
  alias Backend.Accounts.User
  alias CustomerWeb.UserAuth
  # ToDo: This must come from the AppWeb

  require Logger

  def new(conn, _params) do
    changeset = Accounts.change_user_registration(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.register_user(user_params) do
      {:ok, user} ->
        {:ok, _} =
          Accounts.deliver_user_confirmation_instructions(
            user,
            &Routes.user_confirmation_url(conn, :confirm, &1)
          )

        conn
        |> put_flash(
          :info,
          "User created successfully. Please check your email for confirmation instructions."
        )
        # |> UserAuth.log_in_user(user)
        |> redirect(to: Routes.user_session_path(conn, :new))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def delete(conn, _params) do
    current_user = conn.assigns.current_user
    Accounts.delete_user(current_user)

    conn
    |> UserAuth.log_out_user()
    |> redirect(to: Routes.overview_path(conn, :index))
  end
end
