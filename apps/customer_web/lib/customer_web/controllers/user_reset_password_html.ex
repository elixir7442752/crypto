defmodule CustomerWeb.UserResetPasswordHTML do
  use CustomerWeb, :html

  embed_templates "user_reset_password_html/*"
end
