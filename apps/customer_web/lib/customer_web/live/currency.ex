defmodule Currency do
  @btc "BTC"
  @cro "CRO"

  def btc, do: @btc
  def cro, do: @cro
end
