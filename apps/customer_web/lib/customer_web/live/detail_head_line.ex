defmodule DetailHeadLine do
  @moduledoc false

  use Phoenix.LiveComponent
  import Helper

  def render(assigns) do
    ~H"""
    <div class="stats join md:join-horizontal join-vertical w-full shadow-xl flex mt-2 mb-2">
      <div class="stat join-item shadow flex-auto w-16">
        <div class="stat-title">Total balance</div>
        <div class="stat-value text-secondary">
          <%= (sum_native_amount_usd(@purchases) + calculate_gv_in_usd(@purchases, @bitcoin_price_usd))
          |> round(2) %> <%= @symbol %>
        </div>
        <div class="stat-desca">
          <span class="text-base-content/60">Amount: </span><%= sum_to_amount(@purchases) %> BTC
        </div>
      </div>
      <div class="flex-auto w-64 max-md:hidden">
        <.live_component module={CustomerWeb.Performance} id="detailHeadLinePerformance" graphdata={@graph_data}/>
      </div>
    </div>
    """
  end
end
