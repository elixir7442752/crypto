defmodule Helper do
  @moduledoc false

  require Logger

  def round(nil, _precision) do
    0.0
  end

  def round(0, _precision) do
    0.0
  end

  def round(value, precision) do
    round(value, precision, false)
  end

  def round(value, precision, empty_when_zero) do
    case value == 0 && empty_when_zero do
      true -> ""
      false -> :io_lib.format("~." <> Integer.to_string(precision) <> "f", [value])
    end
  end

  def set_text_color(value, opt) when value >= 0 do
    "text-green-400" <> " " <> opt
  end

  def set_text_color(value, opt) when value < 0 do
    "text-red-400" <> " " <> opt
  end

  def setColor(diff_days) when diff_days > 0 do
    "text-red-500"
  end

  def setColor(_diff_days) do
    "text-green-500"
  end

  def set_text_password("text") do
    "text, underline decoration-double"
  end

  def set_text_password("password") do
    "password"
  end

  def calculate_win_lose_usd(_to_amount, _old_coin_price_usd, coin_price_usd)
      when coin_price_usd == 0.0 do
    0.0
  end

  def calculate_win_lose_usd(to_amount, _old_coin_price_usd, _coin_price_usd)
      when to_amount <= 0 do
    0.0
  end

  def calculate_win_lose_usd(to_amount, old_coin_price_usd, coin_price_usd) do
    to_amount * coin_price_usd - to_amount * old_coin_price_usd
  end

  def calculate_win_lose_percentage(_to_amount, _old_coin_price_usd, coin_price_usd)
      when coin_price_usd == 0.0 do
    0.0
  end

  def calculate_win_lose_percentage(to_amount, _old_coin_price_usd, _coin_price_usd)
      when to_amount <= 0 do
    0.0
  end

  def calculate_win_lose_percentage(to_amount, old_coin_price_usd, coin_price_usd) do
    to_amount * coin_price_usd - to_amount * old_coin_price_usd
  end

  def calculate_total_gv_in_usd([], _bitcoin_price_usd) do
    0.0
  end

  def calculate_total_gv_in_usd(purchases, bitcoin_price_usd) do
    Enum.sum(
      for {_type, items} <- purchases,
          do: calculate_gv_in_usd(items, bitcoin_price_usd)
    )
  end

  def calculate_gv_in_usd(purchases_in_btc, bitcoin_price_usd) do
    Enum.sum(
      for item <- purchases_in_btc,
          do:
            calculate_win_lose_usd(
              item.to_amount,
              item.bitcoin_price_usd,
              bitcoin_price_usd
            )
    )
  end

  def sum_to_amount(purchases_in_btc) do
    Enum.sum(for item <- purchases_in_btc, do: item.to_amount)
  end

  def sum_native_amount_usd(purchases_in_btc) do
    Enum.sum(for item <- purchases_in_btc, do: item.native_amount_usd)
  end

  def get_column_title("BTC") do
    "Price"
  end

  def get_column_title("CRO") do
    "CRO Price"
  end

  def get_days(timestamp_utc) do
    365 - Timex.diff(Timex.now(), timestamp_utc, :days)
  end

  def format_date(date) do
    Calendar.strftime(date, "%Y-%m-%d %H:%M:%S")
  end

  def get_user_id(socket) do
    case is_nil(socket.assigns.current_user) do
      false -> socket.assigns.current_user.id
      true -> nil
    end
  end

  def yesterday() do
    Timex.subtract(Timex.today(), Timex.Duration.from_days(1))
  end
end
