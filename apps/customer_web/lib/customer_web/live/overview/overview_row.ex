defmodule CustomerWeb.OverviewRow do
  @moduledoc false
  # use CustomerWeb, :live_view
  use Phoenix.VerifiedRoutes, endpoint: CustomerWeb.Endpoint, router: CustomerWeb.Router
  use Phoenix.LiveComponent
  import Helper

  alias CustomerWeb.OverviewLive

  def render(assigns) do
    ~H"""
    <div class="join md:join-horizontal join-vertical stats w-full shadow-xl mt-2 flex">
      <div class="stat join-item shadow">
        <div class="stat-value text-blue-600 text-2xl">
          <.link patch={~p"/purchases/#{@purchase_type}"}><%= format(@purchase_type) %></.link>
        </div>
        <div class="stat-desca">
          <span class="lg:text-base-content/60">Amount: </span> <%= sum_to_amount(@purchases) %> BTC
        </div>
      </div>
      <div class="stat join-item shadow">
        <div class="stat-title">Purchased</div>
        <div class="stat-title">
          Purchased (USD)
          <span class="text-black"><%= sum_native_amount_usd(@purchases) |> round(2) %> $</span>
        </div>
        <div class="stat-title">
          Purchased (EUR)
          <span class="text-black"><%= sum_native_amount(@purchases) |> round(2) %> €</span>
        </div>
      </div>
      <% wl_in_usd = calculate_gv_in_usd(@purchases, @bitcoin_price_usd) %>
      <% wl_in_percent = calculate_gv_in_percent(wl_in_usd, @purchases) %>
      <div class="stat join-item shadow">
        <div class="stat-title">Win / Loss</div>
        <div class="stat-title">
          <span>W/L($) </span><span class={set_text_color(wl_in_usd, "")}><%= wl_in_usd |> round(2) %> $</span>
        </div>
        <div class="stat-title">
          <span>W/L(%) </span><span class={set_text_color(wl_in_percent, "")}><%= wl_in_percent |> round(2) %> %</span>
        </div>
      </div>
    </div>
    """
  end

  defdelegate calculate_gv_in_percent(wl_in_usd, purchases), to: OverviewLive
  defdelegate format(purchase_tpye), to: OverviewLive
  defdelegate sum_native_amount(purchases), to: OverviewLive
end
