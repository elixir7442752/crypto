defmodule CustomerWeb.OverviewSumTotal do
  @moduledoc false

  use Phoenix.LiveComponent
  import Helper, only: [round: 2, set_text_color: 2]
  alias CustomerWeb.OverviewLive

  def render(assigns) do
    ~H"""
    <div class="stats join md:join-horizontal join-vertical w-full shadow-xl flex mt-2 bg-slate-200">
      <% total_sum_gv = OverviewLive.calculate_total_gv_in_usd(@purchases, @bitcoin_price_usd) %>
      <% total_in_percentage = OverviewLive.calculate_total_in_percent(total_sum_gv, @total_usd) %>

      <div class="stat join-item shadow">
        <div class="stat-title">Total balance</div>
        <div class="stat-value text-secondary"><%= (@total_usd + total_sum_gv) |> round(2) %> $</div>
        <div class="stat-desca">
          <span class="text-base-content/60">Amount: </span><%= @total_amount %> BTC
        </div>
      </div>

      <div class="stat join-item shadow">
        <div class="stat-title ">Purchased (total)</div>
        <div class="stat-title ">
          Purchased (USD) <span class="text-black"><%= @total_usd |> round(2) %> $</span>
        </div>
        <div class="stat-title ">
          Purchased (EUR) <span class="text-black"><%= @total_euro |> round(2) %> €</span>
        </div>
      </div>

      <div class="stat join-item shadow">
        <div class="stat-title">Win / Loss (total)</div>
        <div class="stat-title">
          <span>W/L($) </span><span class={set_text_color(total_sum_gv, "")}><%= total_sum_gv |> round(2) %> $</span>
        </div>
        <div class="stat-title">
          <span>W/L(%) </span><span class={set_text_color(total_in_percentage, "")}><%= total_in_percentage |> round(2) %> %</span>
        </div>
      </div>
    </div>
    """
  end
end
