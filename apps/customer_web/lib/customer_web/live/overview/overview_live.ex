defmodule CustomerWeb.OverviewLive do
  use CustomerWeb, :live_view
  alias Phoenix.PubSub

  alias Backend.Clients.{Bitcoin, Block}
  import Helper
  require Logger

  @impl true
  def mount(_params, session, socket) do
    if connected?(socket), do: PubSub.subscribe(Backend.PubSub, "backend")

    socket =
      socket
      |> MountHelpers.assign_current_user(session)
      |> assign_defaults()
      |> assign_bitcoin_and_cro_price()
      |> assign_purchases()
      |> assign_graph_data()
      |> assign_totals()

    {:ok, socket}
  end

  @impl true
  def handle_info({:bitcoin_price, %{:euro => euro_price, :usd => usd_price}}, socket) do
    case socket.assigns.bitcoin_price_usd == usd_price do
      true ->
        {:noreply, socket}

      false ->
        {:noreply,
         assign(socket,
           old_bitcoin_price_usd: socket.assigns.bitcoin_price_usd,
           bitcoin_price_usd: usd_price,
           old_bitcoin_price_euro: socket.assigns.bitcoin_price_euro,
           bitcoin_price_euro: euro_price,
           total_amount: calculate_total_amount(socket.assigns.purchases),
           total_usd: calculate_total_usd(socket.assigns.purchases),
           total_euro: calculate_total_euro(socket.assigns.purchases),
           page_title: "$ " <> Float.to_string(usd_price) <> " | FiatBitcoin"
         )}
    end
  end

  @impl true
  def handle_info({:cro_price, %{:euro => euro_price, :usd => usd_price}}, socket) do
    case socket.assigns.cro_price_usd == usd_price do
      true ->
        {:noreply, socket}

      false ->
        {:noreply,
         assign(socket,
           cro_price_usd: usd_price,
           cro_price_euro: euro_price
         )}
    end
  end

  @impl true
  def handle_info(
        {:block, %{:block => block, :block_time => block_time, :txs_count => tx_count}},
        socket
      ) do
    {:noreply, assign(socket, block: block, block_time: block_time, tx_count: tx_count)}
  end

  @impl true
  def handle_params(params, _uri, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :chart, _params) do
    socket
    |> assign(:page_title, "Bitcoin Chart")
  end

  defp apply_action(socket, _action, _params) do
    socket
  end

  defp assign_defaults(socket) do
    assign(socket,
      page_title: "FiatBitCoin",
      bitcoin_price_usd: 0.0,
      bitcoin_price_euro: 0.0,
      old_bitcoin_price_usd: 0.0,
      old_bitcoin_price_euro: 0.0,
      cro_price_usd: 0.0,
      cro_price_euro: 0.0,
      button_toogle: false,
      text_type: "text",
      total_amount: 0.0,
      total_usd: 0.0,
      total_euro: 0.0,
      block: 0,
      block_time: 0,
      txs_count: 0
    )
  end

  defp assign_totals(socket) do
    assign(socket,
      total_amount: calculate_total_amount(socket.assigns.purchases),
      total_usd: calculate_total_usd(socket.assigns.purchases),
      total_euro: calculate_total_euro(socket.assigns.purchases)
    )
  end

  def assign_bitcoin_and_cro_price(socket) do
    Bitcoin.trigger()
    Block.trigger()
    socket
  end

  defp assign_purchases(socket) do
    assign(socket, purchases: Backend.get_purchases(get_user_id(socket), "BTC"))
  end

  def assign_graph_data(socket) do
    assign(socket, graph_data: Backend.get_btc_graph(get_user_id(socket), "BTC"))
  end




  def calculate_total_amount(purchases) do
    Enum.sum(for {_type, items} <- purchases, do: sum_to_amount(items))
  end

  def calculate_total_usd(purchases) do
    Enum.sum(for {_type, items} <- purchases, do: sum_native_amount_usd(items))
  end

  def calculate_total_euro(purchases) do
    Enum.sum(for {_type, items} <- purchases, do: sum_native_amount(items))
  end

  def calculate_total_gv_in_usd([], _bitcoin_price_usd) do
    0.0
  end

  def calculate_total_gv_in_usd(purchases, bitcoin_price_usd) do
    Enum.sum(
      for {_type, items} <- purchases,
          do: calculate_gv_in_usd(items, bitcoin_price_usd)
    )
  end

  def calculate_total_in_percent(_total_sum_gv, 0) do
    0.0
  end

  def calculate_total_in_percent(total_sum_gv, total_usd) when is_float(total_sum_gv) do
    total_sum_gv / total_usd * 100
  end

  def calculate_gv_in_percent(_wl_in_usd, []) do
    0.0
  end

  def calculate_gv_in_percent(wl_in_usd, purchases_in_btc) when is_float(wl_in_usd) do
    case sum_native_amount_usd(purchases_in_btc) do
      +0.0 -> 0.0
      _ -> wl_in_usd / sum_native_amount_usd(purchases_in_btc) * 100
    end
  end

  def sum_amount(purchases) do
    Enum.sum(for item <- purchases, do: item.amount)
  end

  def sum_native_amount(purchases_in_btc) do
    Enum.sum(for item <- purchases_in_btc, do: item.native_amount)
  end

  def sum_cro(purchases_in_cro, cro_price) do
    purchases_in_cro * cro_price
  end

  def format(title) do
    String.replace(title, "_", " ")
    |> String.capitalize()
  end

  def is_card_transactions_sum_nil?(%{
        :amount => nil,
        :native_amount => nil,
        :native_amount_usd => nil,
        :timestamp_utc => nil,
        :to_amount => nil
      }) do
    true
  end

  def is_card_transactions_sum_nil?(_) do
    false
  end

  def is_staked_interest_nil?(%{
        :staked_amount => nil,
        :sum_stake_interest => nil,
        :timestamp_utc => nil
      }) do
    true
  end

  def is_staked_interest_nil?(_) do
    false
  end

  def is_card_cashback_sum?([]) do
    true
  end

  def is_card_cashback_sum?(_) do
    false
  end
end
