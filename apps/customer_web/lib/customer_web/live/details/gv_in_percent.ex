defmodule GvInPercent do
  @moduledoc false
  use Phoenix.LiveComponent

  def render(assigns) do
    ~H"""
    <p class={set_text_color(@item_to_amount, @item_coin_price_usd, @coin_price_usd, @text_type)}>
      <%= if @item_native_amount_usd == 0 do %>
        0.00
      <% else %>
        <%= (Helper.calculate_win_lose_percentage(
               @item_to_amount,
               @item_coin_price_usd,
               @coin_price_usd
             ) / @item_native_amount_usd * 100)
        |> Helper.round(2) %>
      <% end %>
    </p>
    """
  end

  def set_text_color(item_to_amount, item_coin_price_usd, coin_price_usd, text_type)
      when item_to_amount * coin_price_usd - item_to_amount * item_coin_price_usd >= 0 do
    "text-green-500" <> " " <> text_type
  end

  def set_text_color(item_to_amount, item_coin_price_usd, coin_price_usd, text_type)
      when item_to_amount * coin_price_usd - item_to_amount * item_coin_price_usd < 0 do
    "text-red-500" <> " " <> text_type
  end
end
