defmodule CustomerWeb.ManualsLive do
  use CustomerWeb, :live_view

  alias Phoenix.PubSub
  alias Backend.Data.ManualPurchase
  alias Backend.Clients.Bitcoin
  import Helper, only: [get_user_id: 1]
  require Logger

  @impl true
  def mount(_params, session, socket) do
    init(socket, session, "Manual", "BTC")
  end

  @impl true
  def handle_params(params, _uri, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Manual purchases")
    |> assign(
      manual_purchases: Backend.DataRepository.get_manual_purchases(get_user_id(socket), "BTC")
    )
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "Create new Manual purchases")
    |> assign(
      manual_purchase: %ManualPurchase{
        user_id: socket.assigns.current_user.id,
        bitcoin_price_usd: socket.assigns.bitcoin_price_usd,
        bitcoin_price_euro: socket.assigns.bitcoin_price_euro
      }
    )
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Manual purchase")
    |> assign(:manual_purchase, Backend.DataRepository.get_manual_purchase(id))
  end

  @impl true
  def handle_info({:bitcoin_price, %{:euro => euro_price, :usd => usd_price}}, socket) do
    case socket.assigns.bitcoin_price_usd == usd_price do
      true ->
        {:noreply, socket}

      false ->
        {
          :noreply,
          assign(
            socket,
            old_bitcoin_price_usd: socket.assigns.bitcoin_price_usd,
            bitcoin_price_usd: usd_price,
            old_bitcoin_price_euro: socket.assigns.bitcoin_price_euro,
            bitcoin_price_euro: euro_price
          )
        }
    end
  end

  @impl true
  def handle_info({CustomerWeb.ManualFormComponent, {:saved, _manual_purchase}}, socket) do
    socket =
      socket
      |> assign(
        manual_purchases: Backend.DataRepository.get_manual_purchases(get_user_id(socket), "BTC")
      )

    {:noreply, socket}
  end

  @impl true
  def handle_info(_params, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_event("toggle_hidden", _params, socket) do
    case socket.assigns.button_toogle do
      true -> {:noreply, assign(socket, button_toogle: false, text_type: "text")}
      false -> {:noreply, assign(socket, button_toogle: true, text_type: "password")}
    end
  end

  defp init(socket, session, headline, currency) do
    if connected?(socket), do: PubSub.subscribe(Backend.PubSub, "backend")

    socket =
      socket
      |> MountHelpers.assign_current_user(session)
      |> assign_defaults(headline, currency)
      |> assign_bitcoin_price()

    {:ok, socket}
  end

  defp assign_bitcoin_price(socket) do
    Bitcoin.trigger()
    socket
  end

  defp assign_defaults(socket, headline, currency) do
    assign(socket,
      page_title: headline,
      currency: currency,
      old_bitcoin_price_usd: 0.0,
      bitcoin_price_usd: 0.0,
      old_bitcoin_price_euro: 0.0,
      bitcoin_price_euro: 0.0,
      cro_price_euro: 0.0,
      cro_price_usd: 0.0,
      headline: headline,
      button_toogle: false,
      text_type: "text",
      manual_purchases: []
    )
  end
end
