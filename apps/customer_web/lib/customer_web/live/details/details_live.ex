defmodule CustomerWeb.DetailsLive do
  use CustomerWeb, :live_view

  alias Phoenix.PubSub
  alias Backend.BtcPriceRepo
  alias Backend.Clients.Bitcoin
  import Helper
  require Logger

  @impl true
  def mount(%{"type" => "crypto_purchases"}, session, socket) do
    init(socket, session, "Crypto Purchases", "BTC")
  end

  @impl true
  def mount(%{"type" => "recurring_buy_orders"}, session, socket) do
    init(socket, session, "Recurring buy orders", "BTC")
  end

  @impl true
  def mount(%{"type" => "viban_purchases"}, session, socket) do
    init(socket, session, "Viban Purchases", "BTC")
  end

  @impl true
  def mount(%{"type" => "crypto_earns"}, session, socket) do
    init(socket, session, "Crypto Earns", "BTC")
  end

  @impl true
  def mount(%{"type" => "card_cashback"}, session, socket) do
    init(socket, session, "Card Cashback", "CRO")
  end

  @impl true
  def mount(%{"type" => "card_transactions"}, session, socket) do
    init(socket, session, "Card Transactions", nil)
  end

  @impl true
  def mount(%{"type" => "stake_interest"}, session, socket) do
    init(socket, session, "Stake Interest", "CRO")
  end

  @impl true
  def mount(%{"type" => "etoro_gbt"}, session, socket) do
    init(socket, session, "Etoro", "BTC")
  end

  @impl true
  def mount(%{"type" => "pocket_purchases"}, session, socket) do
    init(socket, session, "Pocket", "BTC")
  end

  @impl true
  def mount(_params, session, socket) do
    init(socket, session, "Recurring buy orders", "BTC")
  end

  @impl true
  def handle_params(%{"type" => "crypto_purchases" = type}, _uri, socket) do
    purchases = Backend.DataRepository.get_crypto_purchases(get_user_id(socket), "BTC")

    {:noreply,
     assign(socket,
       crypto_purchases: purchases,
       type: type,
       db_bitcoin_price_list: get_missing_bitcoin_prices(purchases)
     )}
  end

  @impl true
  def handle_params(%{"type" => "recurring_buy_orders" = type}, _uri, socket) do
    purchases = Backend.DataRepository.get_recurring_buy_orders(get_user_id(socket), "BTC")

    {
      :noreply,
      assign(socket,
        crypto_purchases: purchases,
        type: type,
        db_bitcoin_price_list: get_missing_bitcoin_prices(purchases)
      )
    }
  end

  @impl true
  def handle_params(%{"type" => "viban_purchases" = type}, _uri, socket) do
    purchases = Backend.DataRepository.get_viban_purchases(get_user_id(socket), "BTC")

    {:noreply,
     assign(socket,
       crypto_purchases: purchases,
       type: type,
       db_bitcoin_price_list: get_missing_bitcoin_prices(purchases)
     )}
  end

  @impl true
  def handle_params(%{"type" => "crypto_earns" = type}, _uri, socket) do
    purchases = Backend.DataRepository.get_crypto_earns(get_user_id(socket), "BTC")

    {:noreply,
     assign(socket,
       crypto_purchases: purchases,
       type: type,
       db_bitcoin_price_list: get_missing_bitcoin_prices(purchases)
     )}
  end

  @impl true
  def handle_params(%{"type" => "etoro_gbt" = type}, _uri, socket) do
    purchases = Backend.DataRepository.get_etoro_gbt(get_user_id(socket), "BTC")

    {:noreply,
     assign(socket,
       crypto_purchases: purchases,
       type: type,
       db_bitcoin_price_list: get_missing_bitcoin_prices(purchases)
     )}
  end

  @impl true
  def handle_params(%{"type" => "pocket_purchases" = type}, _uri, socket) do
    purchases = Backend.DataRepository.get_pocket_purchases(get_user_id(socket), "BTC")

    {:noreply,
     assign(socket,
       crypto_purchases: purchases,
       type: type,
       graph_data: Backend.get_btc_graph(get_user_id(socket), "BTC")
     )}
  end

  @impl true
  def handle_params(params, _uri, socket) do
    Logger.error("got request i can't handle: #{inspect(params)}")
    {:noreply, socket}
  end

  @impl true
  def handle_info({:bitcoin_price, %{:euro => euro_price, :usd => usd_price}}, socket) do
    {
      :noreply,
      assign(
        socket,
        old_bitcoin_price_usd: socket.assigns.bitcoin_price_usd,
        bitcoin_price_usd: usd_price,
        old_bitcoin_price_euro: socket.assigns.bitcoin_price_euro,
        bitcoin_price_euro: euro_price
      )
    }
  end

  @impl true
  def handle_info({:cro_price, %{:euro => euro_price, :usd => usd_price}}, socket) do
    case socket.assigns.cro_price_usd == usd_price do
      true ->
        {:noreply, socket}

      false ->
        {:noreply,
         assign(socket,
           cro_price_usd: usd_price,
           cro_price_euro: euro_price
         )}
    end
  end

  @impl true
  def handle_info(_params, socket) do
    {:noreply, socket}
  end

  defp init(socket, session, headline, currency) do
    if connected?(socket), do: PubSub.subscribe(Backend.PubSub, "backend")

    socket =
      socket
      |> MountHelpers.assign_current_user(session)
      |> assign_defaults(headline, currency)
      |> assign_bitcoin_price()

    {:ok, socket}
  end

  defp assign_bitcoin_price(socket) do
    Bitcoin.trigger()
    socket
  end

  defp assign_defaults(socket, headline, currency) do
    assign(socket,
      page_title: headline,
      currency: currency,
      old_bitcoin_price_usd: 0.0,
      bitcoin_price_usd: 0.0,
      old_bitcoin_price_euro: 0.0,
      bitcoin_price_euro: 0.0,
      cro_price_euro: 0.0,
      cro_price_usd: 0.0,
      headline: headline,
      button_toogle: false,
      text_type: "text",
      crypto_purchases: [],
      db_bitcoin_price_list: []
    )
  end

  defp get_missing_bitcoin_prices(purchases) do
    last_p = List.last(purchases)
    BtcPriceRepo.load_existing_dates_and_amounts(Timex.to_date(last_p.timestamp_utc), yesterday())
  end
end
