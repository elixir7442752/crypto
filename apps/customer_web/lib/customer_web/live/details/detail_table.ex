defmodule DetailTable do
  use Phoenix.LiveComponent

  require Logger

  import Helper

  @impl true
  def mount(socket) do
    {:ok,
     assign(socket,
       order: "desc"
     )}
  end

  @impl true
  def handle_event("sort_by", %{"order" => order, "field" => field}, socket) do
    order =
      case order do
        "desc" -> "asc"
        "asc" -> "desc"
      end

    {:noreply,
     assign(socket,
       purchases: sort_by(socket.assigns.purchases, field, String.to_atom(order)),
       order: order
     )}
  end

  defp sort_by(purchases, "amount", order) do
    Enum.sort_by(purchases, &{&1.to_amount}, order)
  end

  defp sort_by(purchases, "timestamp_utc", order) do
    Enum.sort_by(purchases, &{&1.timestamp_utc}, order)
  end

  defp sort_by(purchases, "native_amount_usd", order) do
    Enum.sort_by(purchases, &{&1.native_amount_usd}, order)
  end
end
