defmodule CustomerWeb.ManualFormComponent do
  use CustomerWeb, :live_component
  alias Backend.Clients.UsdEuro
  require Logger

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
      </.header>

      <.simple_form
        for={@form}
        id="manual_purchase-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input type="datetime-local" field={@form[:timestamp_utc]} label="Time" />
        <.input field={@form[:transaction_description]} label="Description" />
        <.input type="number" step="0.00001" field={@form[:to_amount]} label="Amount" />
        <.input type="number" field={@form[:native_amount_usd]} label="bought USD" readonly />
        <.input type="number" field={@form[:native_amount]} label="bought Euro" readonly />
        <.input type="hidden" field={@form[:amount]} />
        <.input type="number" field={@form[:bitcoin_price_usd]} label="BTC Price (USD)" readonly />
        <.input type="number" field={@form[:bitcoin_price_euro]} label="BTC Price (Euro)" readonly />
        <.input type="hidden" field={@form[:user_id]} />
        <:actions>
          <.button
            phx-disable-with="Saving..."
            class="bg-blue-600 hover:bg-blue-800 text-white font-bold py-2 px-4 rounded"
          >
            Save
          </.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{manual_purchase: manual_purchase} = assigns, socket) do
    changeset = Backend.DataRepository.change_manual_purchase(manual_purchase)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"manual_purchase" => manual_purchase_params}, socket) do
    to_amount = Map.get(manual_purchase_params, "to_amount")

    bitcoin_price_usd =
      get_bitcoin_price(
        Map.get(manual_purchase_params, "timestamp_utc"),
        to_float(Map.get(manual_purchase_params, "bitcoin_price_usd"))
      )

    bitcoin_price_euro = UsdEuro.get_factor() * bitcoin_price_usd
    native_amount_usd = price(to_float(bitcoin_price_usd), to_float(to_amount))
    native_amount = price(to_float(bitcoin_price_euro), to_float(to_amount))

    manual_purchase_params =
      manual_purchase_params
      |> Map.put("native_amount_usd", native_amount_usd)
      |> Map.put("native_amount", native_amount)
      |> Map.put("amount", to_amount)
      |> Map.put("bitcoin_price_usd", bitcoin_price_usd)
      |> Map.put("bitcoin_price_euro", bitcoin_price_euro |> Helper.round(2))

    changeset =
      socket.assigns.manual_purchase
      |> Backend.DataRepository.change_manual_purchase(manual_purchase_params)

    {:noreply, assign_form(socket, changeset)}
  end

  @impl true
  def handle_event("save", %{"manual_purchase" => manual_purchase_params}, socket) do
    save_manual_purchase(socket, socket.assigns.action, manual_purchase_params)
  end

  def get_bitcoin_price(datetime, bitcoin_price_usd_actual) do
    case Backend.get_bitcoin_price(to_DateTime(datetime)) do
      nil -> bitcoin_price_usd_actual
      bitcoin_price -> bitcoin_price.amount
    end
  end

  defp save_manual_purchase(socket, :edit, manual_purchase_params) do
    case Backend.DataRepository.update_manual_purchase(
           socket.assigns.manual_purchase,
           manual_purchase_params
         ) do
      {:ok, manual_purchase} ->
        notify_parent({:saved, manual_purchase})

        {:noreply,
         socket
         |> put_flash(:info, "Manual purchase updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_manual_purchase(socket, :new, manual_purchase_params) do
    date = Map.get(manual_purchase_params, "timestamp_utc")

    manual_purchase_params =
      manual_purchase_params
      |> Map.put("timestamp_utc", to_DateTime(date))
      |> Map.put("currency", "EUR")
      |> Map.put("native_currency", "EUR")
      |> Map.put("to_currency", "BTC")

    case Backend.DataRepository.create_manual_purchase(manual_purchase_params) do
      {:ok, manual_purchase} ->
        notify_parent({:saved, manual_purchase})

        {:noreply,
         socket
         |> put_flash(:info, "Manual purchase created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp price(factor_1, factor_2) do
    (factor_1 * factor_2) |> Helper.round(2)
  end

  defp to_float("") do
    0.0
  end

  defp to_float(value) when is_float(value) do
    value
  end

  defp to_float(string) do
    case Float.parse(string) do
      {float, _rest} -> float
      :error -> 0
    end
  end

  defp to_DateTime(date_time) do
    {:ok, date_time, 0} = DateTime.from_iso8601(date_time <> ":00Z")
    date_time
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
