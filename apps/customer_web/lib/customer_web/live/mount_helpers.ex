defmodule MountHelpers do
  import Phoenix.Component

  alias Backend.Accounts
  @moduledoc false

  def assign_defaults(socket, _params, session, _acl) do
    socket
    |> assign_current_user(session)
  end

  def assign_current_user(socket, session) do
    assign_new(socket, :current_user, fn ->
      Accounts.get_user_by_session_token(session["user_token"])
    end)
  end
end
