defmodule CustomerWeb.Upload.UploadLive do
  @moduledoc """
  Demonstrates automatic uploads with the Phoenix Channels uploader.
  """
  use CustomerWeb, :live_view
  import Helper, only: [get_user_id: 1]

  alias Backend
  require Logger

  @impl true
  def mount(_params, session, socket) do
    {:ok,
     socket
     |> MountHelpers.assign_current_user(session)
     |> assign(:uploaded_files, [])
     |> allow_upload(:exhibit,
       accept: ~w(.csv),
       max_entries: 1,
       max_file_size: 100_000_000,
       chunk_size: 256,
       auto_upload: true,
       progress: &handle_progress/3
     )}
  end

  # with auto_upload: true we can consume files here
  defp handle_progress(:exhibit, entry, socket) do
    if entry.done? do
      consume_uploaded_entry(socket, entry, fn meta ->
        socket.assigns.uploads.exhibit

        dest_file =
          Path.join([:code.priv_dir(:customer_web), "static", "uploads", "#{entry.client_name}"])

        File.cp!(meta.path, dest_file)

        case Backend.parse_and_persist(get_user_id(socket), dest_file) do
          {:error, reason} -> Logger.error("error processing file: #{reason}")
          :ok -> Logger.info("finished: upload")
        end

        File.rm(dest_file)
        {:ok, entry.uuid}
      end)

      {:noreply, update(socket, :uploaded_files, &[entry.client_name | &1])}
    else
      socket
      |> put_flash(:info, "User confirmed successfully.")

      {:noreply, socket}
    end
  end

  @impl true
  def handle_event("validate", _params, socket) do
    Logger.info("---- #{inspect(socket)}")
    {:noreply, socket}
  end

  @impl true
  def handle_event("cancel-upload", %{"ref" => ref}, socket) do
    {:noreply, cancel_upload(socket, :exhibit, ref)}
  end

  def error_to_string(:too_large), do: "Too large"
  def error_to_string(:not_accepted), do: "You have selected an unacceptable file type"
  def error_to_string(:too_many_files), do: "You have selected too many files"

  @impl true
  def handle_params(_params, _uri, socket) do
    {:noreply, socket}
  end
end
