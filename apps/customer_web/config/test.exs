use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :customer_web, CustomerWeb.Endpoint,
  http: [port: 4002],
  server: false

# Bamboo configuration
config :backend, CustomerWeb.Mailer, adapter: Bamboo.LocalAdapter

config :phoenix_test, :endpoint, CustomerWeb.Endpoint
