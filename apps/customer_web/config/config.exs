# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of the Config module.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
import Config

config :customer_web,
  generators: [context_app: :backend]

# Configures the endpoint
config :customer_web, CustomerWeb.Endpoint,
  url: [host: "localhost"],
  adapter: Phoenix.Endpoint.Cowboy2Adapter,
  # secret_key_base: "vCjfhejBPZz5I7gt3swvBeuifUggFppjvYb2hiKSniYnNdeKg49JwbJfkvHheKRZ",
  # render_errors: [view: CustomerWeb.ErrorView, accepts: ~w(html json), layout: false],
  render_errors: [
    formats: [html: TestWeb.ErrorHTML, json: TestWeb.ErrorJSON],
    layout: false
  ],
  pubsub_server: CustomerWeb.PubSub,
  live_view: [signing_salt: "FbswchPf"],
  server: true

config :tailwind,
  version: "3.2.4",
  customer_web: [
    args: ~w(
            --config=tailwind.config.js
            --input=../assets/css/app.css
            --output=../priv/static/assets/app.css
          ),
    cd: Path.expand("../assets", __DIR__)
  ]

config :esbuild,
  version: "0.17.11",
  customer_web: [
    args: ~w(js/app.js
           --bundle
           --target=es2017
           --outdir=../priv/static/assets
           --external:/fonts/*
           --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../../../deps", __DIR__)}
  ]

# Sample configuration:
#
#     config :logger, :console,
#       level: :info,
#       format: "$date $time [$level] $metadata$message\n",
#       metadata: [:user_id]
#

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
