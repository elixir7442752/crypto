defmodule Backend do
  alias Backend.Parser
  alias Backend.DataRepository
  alias Backend.BtcPriceRepo
  alias Backend.FileType
  alias BtcGraphData

  require Logger

  @geschlossene_positionen_table "geschlossene positionen-table"
  @pocket "pocket"
  @crypto_transactions "crypto_transactions_record"
  @krypto_transactions "krypto_transactions_record"

  def parse_and_persist(user_id, filename) do
    case parse(user_id, filename) do
      {:error, a} ->
        Logger.error("file with filename: #{filename} is not suppported!")
        {:error, a}

      _ ->
        for entity <- parse(user_id, filename), do: save(entity)
        :ok
    end
  end

  def parse(user_id, filename) do
    cond do
      is_etoro_file?(filename) ->
        Parser.parse(user_id, %FileType{kind: FileType.etoro(), filename: filename})

      is_pocket_file?(filename) ->
        Parser.parse(user_id, %FileType{kind: FileType.pocket(), filename: filename})

      is_crypto_transactions_file?(filename) ->
        Parser.parse(user_id, %FileType{kind: FileType.crypto_transactions(), filename: filename})

      is_krypto_transactions_file?(filename) ->
        Parser.parse(user_id, %FileType{kind: FileType.krypto_transactions(), filename: filename})

      is_no_supported_file?(filename) ->
        {:error, "file not supported"}
    end
  end

  def is_crypto_transactions_file?(filename) do
    which_file?(filename, @crypto_transactions)
  end

  def is_krypto_transactions_file?(filename) do
    which_file?(filename, @krypto_transactions)
  end

  def is_etoro_file?(filename) do
    which_file?(filename, @geschlossene_positionen_table)
  end

  def is_pocket_file?(filename) do
    which_file?(filename, @pocket)
  end

  def is_no_supported_file?(_filename) do
    true
  end

  def which_file?(filename, name) do
    Path.basename(filename)
    |> String.downcase()
    |> String.contains?(name)
  end

  def downcase_filename(filename) do
    Path.basename(filename, ".csv")
    |> String.downcase()
  end


  def get_rbo_sum(user_id, currency) do
    [amount, native_amount_usd, to_amount] = DataRepository.get_rbo_sum(user_id, currency)
    %{amount: amount, native_amount_usd: native_amount_usd, to_amount: to_amount}
  end

  def get_vp_sum(user_id, currency) do
    [amount, native_amount_usd, to_amount] = DataRepository.get_vp_sum(user_id, currency)
    %{amount: amount, native_amount_usd: native_amount_usd, to_amount: to_amount}
  end

  def get_cp_sum(user_id, currency) do
    [amount, native_amount_usd, to_amount] = DataRepository.get_cp_sum(user_id, currency)
    %{amount: amount, native_amount_usd: native_amount_usd, to_amount: to_amount}
  end

  def get_bitcoin_price(datetime) do
    BtcPriceRepo.get_bitcoin_price_for_date(Timex.to_date(datetime))
  end

  def get_purchases(nil, _currency) do
    []
  end

  def get_purchases(user_id, currency) do
    [
      {"viban_purchases", DataRepository.get_viban_purchases(user_id, currency)},
      {"crypto_purchases", DataRepository.get_crypto_purchases(user_id, currency)},
      {"recurring_buy_orders", DataRepository.get_recurring_buy_orders(user_id, currency)},
      {"crypto_earns", DataRepository.get_crypto_earns(user_id, currency)},
      {"etoro_gbt", DataRepository.get_etoro_gbt(user_id, currency)},
      {"pocket_purchases", DataRepository.get_pocket_purchases(user_id, currency)},
      {"manual_purchases", DataRepository.get_manual_purchases(user_id, currency)}
    ]
  end

  def get_btc_graph(nil, _currency) do
    []
  end
  def get_btc_graph(user_id, currency) do
    purchases = get_btc_graph_data(user_id, currency)

    {:ok, %{euro: _bitcoin_price_euro, usd: bitcoin_price_usd}} =
      Backend.Clients.Bitcoin.get_price()

    bitcoin_price_list = case List.first(purchases) do
        nil -> []
        first_purchase -> BtcPriceRepo.load_existing_dates_and_amounts(BtcGraphData.to_date(first_purchase.timestamp_utc),BtcGraphData.yesterday())
    end

    BtcGraphData.cumulative_sum(
      purchases,
      %{date: 0, value: 0.0, sum_amount: 0.0},
      bitcoin_price_usd,
      [],
      bitcoin_price_list
    )
  end

  defp get_btc_graph_data(user_id, currency) do
    (DataRepository.get_viban_purchases(user_id, currency) ++
       DataRepository.get_crypto_purchases(user_id, currency) ++
       DataRepository.get_recurring_buy_orders(user_id, currency) ++
       DataRepository.get_crypto_earns(user_id, currency) ++
       DataRepository.get_etoro_gbt(user_id, currency) ++
       DataRepository.get_pocket_purchases(user_id, currency) ++
       DataRepository.get_manual_purchases(user_id, currency))
    |> Enum.sort_by(& &1.timestamp_utc, {:asc, DateTime})
  end

  defp save(entity) do
    DataRepository.save(entity)
  end
end
