defmodule Backend.Parser do
  alias Backend.Factory
  alias Backend.Clients.UsdEuro
  require Logger

  NimbleCSV.define(CSV, separator: ",", escape: "\"")
  NimbleCSV.define(SSV, separator: ";", escape: "\"")

  def parse(user_id, %{kind: :stake_interest, filename: filename}) do
    filename
    |> File.stream!()
    |> CSV.parse_stream()
    |> Stream.map(fn [
                       timestamp_utc,
                       staked_currency,
                       staked_amount,
                       apr,
                       interest_currency,
                       interest_amount,
                       status
                     ] ->
      Factory.create(%{
        user_id: user_id,
        timestamp_utc: to_datetime_1(timestamp_utc),
        staked_currency: staked_currency,
        staked_amount: to_float(staked_amount),
        apr: apr,
        interest_currency: interest_currency,
        interest_amount: to_float(interest_amount),
        status: status,
        transaction_kind: "stake_interest",
        cro_price_usd: 0.0,
        cro_price_euro: 0.0
      })
    end)
    |> Stream.filter(fn x -> x != [] end)
    |> Enum.to_list()
  end

  # etoro file
  def parse(user_id, %{kind: :etoro, filename: filename}) do
    filename
    |> File.stream!()
    |> SSV.parse_stream()
    |> Stream.map(fn [
                       _postion_id,
                       aktion,
                       betrag,
                       einheiten,
                       eroffnungsdatum,
                       _schliessungsdatum,
                       _hebel,
                       spread,
                       _gewinn,
                       eroffnungspreis,
                       _schliessungspreis,
                       _take_profit_preis,
                       _stop_loss_preis,
                       _rollover_gebuhren_und_dividenden,
                       _kopiert_von,
                       _art,
                       _isin,
                       _hinweise,
                       _leer
                     ] ->
      Factory.create(%{
        user_id: user_id,
        timestamp_utc: to_datetime_2(eroffnungsdatum),
        transaction_description: to_transaction_description(aktion),
        currency: "USD",
        amount: to_amount(betrag, :non_native),
        to_currency: to_currency(aktion),
        to_amount: to_float(einheiten),
        native_currency: "EUR",
        native_amount: to_amount(betrag, :native),
        native_amount_usd: to_amount(betrag, :native_usd),
        transaction_kind: "etoro_gbt",
        bitcoin_price_usd: convert_eroffnungspreis(eroffnungspreis),
        bitcoin_price_euro: 0.0,
        spread: String.to_float(String.trim(spread))
      })
    end)
    |> Stream.filter(fn x -> x != [] end)
    |> Enum.to_list()
  end

  def parse(user_id, %{kind: :pocket, filename: filename}) do
    filename
    |> File.stream!()
    |> CSV.parse_stream()
    |> Stream.map(fn [
                       type,
                       date,
                       _reference,
                       _price_currency,
                       price_amount,
                       cost_currency,
                       cost_amount,
                       fee_currency,
                       fee_amount,
                       value_currency,
                       value_amount
                     ] ->
      Factory.create(%{
        user_id: user_id,
        timestamp_utc: date,
        transaction_description: type,
        currency: cost_currency,
        amount: to_float(cost_amount) * -1,
        to_currency: value_currency,
        to_amount: to_float(value_amount),
        native_currency: "EUR",
        native_amount: to_float(cost_amount),
        native_amount_usd: to_float(cost_amount) / get_usd_to_euro(),
        transaction_kind: "pocket",
        bitcoin_price_usd: to_float(price_amount) / get_usd_to_euro(),
        bitcoin_price_euro: to_float(price_amount),
        fee_currency: fee_currency,
        fee_amount: to_float(fee_amount)
      })
    end)
    |> Stream.filter(fn x -> x != [] end)
    |> Enum.to_list()
  end

  def parse(user_id, %{kind: :krypto_transactions} = file_type) do
    parse(user_id, %{file_type | kind: :crypto_transactions})
  end

  def parse(user_id, %{kind: :crypto_transactions, filename: filename}) do
    filename
    |> File.stream!()
    |> CSV.parse_stream()
    |> Stream.map(fn [
                       timestamp_utc,
                       transaction_description,
                       currency,
                       amount,
                       to_currency,
                       to_amount,
                       native_currency,
                       native_amount,
                       native_amount_usd,
                       transaction_kind,
                       _transaction_hash
                     ] ->
      Factory.create(%{
        user_id: user_id,
        timestamp_utc: to_datetime(timestamp_utc),
        transaction_description: transaction_description,
        currency: currency,
        amount: to_float(amount),
        to_currency: to_currency,
        to_amount: to_float(to_amount),
        native_currency: native_currency,
        native_amount: to_float(native_amount),
        native_amount_usd: to_float(native_amount_usd),
        transaction_kind: transaction_kind,
        bitcoin_price_usd: 0.0,
        bitcoin_price_euro: 0.0,
        cro_price_usd: 0.0,
        cro_price_euro: 0.0
      })
    end)
    |> Stream.filter(fn x -> x != [] end)
    |> Enum.to_list()
  end

  def parse(user_id, %{kind: :card_transactions, filename: filename}) do
    filename
    |> File.stream!()
    |> CSV.parse_stream()
    |> Stream.map(fn [
                       timestamp_utc,
                       transaction_description,
                       currency,
                       amount,
                       to_currency,
                       to_amount,
                       native_currency,
                       native_amount,
                       native_amount_usd,
                       _transaction_kind,
                       _transaction_hash
                     ] ->
      Factory.create(%{
        user_id: user_id,
        timestamp_utc: to_datetime(timestamp_utc),
        transaction_description: transaction_description,
        currency: currency,
        amount: to_float(amount),
        to_currency: to_currency,
        to_amount: to_float(to_amount),
        native_currency: native_currency,
        native_amount: to_float(native_amount),
        native_amount_usd: to_float(native_amount_usd),
        transaction_kind: "card_transaction",
        bitcoin_price_usd: 0.0,
        bitcoin_price_euro: 0.0,
        cro_price_usd: 0.0,
        cro_price_euro: 0.0
      })
    end)
    |> Stream.filter(fn x -> x != [] end)
    |> Enum.to_list()
  end

  # the eroffnungspreis has the format "4.000,35"
  defp convert_eroffnungspreis(eroffnungspreis) do
    to_float(String.replace(String.replace(eroffnungspreis, ".", ""), ",", "."))
  end

  defp to_amount("", _) do
    ""
  end

  defp to_amount(betrag, :non_native) do
    String.to_float(String.replace(String.trim(betrag), ".", "")) * -1
  end

  defp to_amount(betrag, :native_usd) do
    String.to_float(String.replace(String.trim(betrag), ".", ""))
  end

  defp to_amount(betrag, :native) do
    String.to_float(String.replace(String.trim(betrag), ".", "")) * get_usd_to_euro()
  end

  defp to_transaction_description("Buy Bitcoin") do
    "USD -> BTC"
  end

  defp to_transaction_description("Buy Cardano") do
    "USD -> ADA"
  end

  defp to_transaction_description("Buy Stellar") do
    "USD -> XLM"
  end

  defp to_currency("Buy Bitcoin") do
    "BTC"
  end

  defp to_currency("Buy Cardano") do
    "ADA"
  end

  defp to_currency("Buy Stellar") do
    "XLM"
  end

  def to_float("") do
    0.0
  end

  def to_float(nil) do
    0.0
  end

  def to_float(value) do
    {float, _rest} = Float.parse(value)
    float
  end

  defp to_datetime(value) do
    {:ok, datetime} = Timex.parse(value, "{YYYY}-{0M}-{0D} {h24}:{m}:{s}")
    Timex.to_datetime(datetime)
  end

  defp to_datetime_1(value) do
    {:ok, datetime} = Timex.parse(value, "{YYYY}-{0M}-{0D} {h24}:{m}:{s}.000")
    Timex.to_datetime(datetime)
  end

  defp to_datetime_2(value) do
    {:ok, datetime} = Timex.parse(value, "{0D}/{0M}/{YYYY} {h24}:{m}:{s}")
    Timex.to_datetime(datetime)
  end

  def calculate_bitcoin_price_in_usd("BTC", to_amount, native_amount_usd) do
    native_amount_usd / to_amount
  end

  def calculate_bitcoin_price_in_usd(_other, _to_amount, _amount) do
    0.0
  end

  def calculate_bitcoin_price_in_euro("BTC", to_amount, native_amount) do
    native_amount / to_amount
  end

  def calculate_bitcoin_price_in_euro(_other, _to_amount, _native_amount) do
    0.0
  end

  def get_usd_to_euro() do
    UsdEuro.get_factor()
  end
end
