defmodule BtcGraphData do
  @moduledoc false

  def cumulative_sum(
        [],
        %{date: 0, value: +0.0, sum_amount: +0.0},
        _bitcoin_price,
        [],
        _bitcoin_price_list
      )do
    []
    end

    def cumulative_sum([], last, bitcoin_price, acc, bitcoin_price_list) do
    acc =
      acc ++
        fill_if_missing_dates(
          bitcoin_price_list,
          last.sum_amount,
          to_date(last.date),
          yesterday()
        )

    [
      %{date: now(), value: value(bitcoin_price, last.sum_amount), sum_amount: last.sum_amount}
      | acc
    ]
  end

  def cumulative_sum(
        [purchase, next | purchases],
        %{date: 0, value: +0.0, sum_amount: +0.0},
        bitcoin_price,
        [],
        bitcoin_price_list
      ) do
    new = %{
      date: to_date(purchase.timestamp_utc),
      value: value(purchase.bitcoin_price_usd, purchase.to_amount),
      sum_amount: purchase.to_amount
    }

    acc = [
      new
      | fill_if_missing_dates(
          bitcoin_price_list,
          new.sum_amount,
          next.timestamp_utc,
          purchase.timestamp_utc
        )
    ]

    cumulative_sum([next] ++ purchases, new, bitcoin_price, acc, bitcoin_price_list)
  end

  def cumulative_sum([purchase | purchases], last, bitcoin_price, acc, bitcoin_price_list) do
    case Date.compare(to_date(purchase.timestamp_utc), to_date(last.date)) == :eq do
      true ->
        acc = List.delete(acc, last)

        new = %{
          date: last.date,
          value: value(purchase.bitcoin_price_usd, purchase.to_amount + last.sum_amount),
          sum_amount: last.sum_amount + purchase.to_amount
        }

        cumulative_sum(purchases, new, bitcoin_price, acc, bitcoin_price_list)

      false ->
        new = %{
          date: to_date(purchase.timestamp_utc),
          value: value(purchase.bitcoin_price_usd, purchase.to_amount + last.sum_amount),
          sum_amount: purchase.to_amount + last.sum_amount
        }

        acc =
          acc ++
            fill_if_missing_dates(
              bitcoin_price_list,
              last.sum_amount,
              last.date,
              purchase.timestamp_utc
            )

        cumulative_sum(purchases, new, bitcoin_price, [new | acc], bitcoin_price_list)
    end
  end

  def create_missing_bitcoin_list([], _last_date, _sum_amount) do
    []
  end

  def create_missing_bitcoin_list(bitcoin_price_list, _last_date, sum_amount) do
    Enum.map(bitcoin_price_list, fn x ->
      %{date: x.date, value: value(x.amount, sum_amount), sum_amount: sum_amount}
    end)
  end

  def get_bitcoin_price_list_slice(bitcon_price_list, from_date, to_date) do
    from_index = from_index(bitcon_price_list, to_date(from_date))
    amount = amount(bitcon_price_list, from_index, to_date(to_date))
    get_bitcoin_price_list_slice_by_index(bitcon_price_list, from_index, amount)
  end

  def get_bitcoin_price_list_slice_by_index(bitcon_price_list, from_index, amount) do
    Enum.slice(bitcon_price_list, from_index, amount)
  end

  def fill_if_missing_dates(bitcon_price_list, sum_amount, last_date, actual_date) do
    actual_date = to_date(actual_date)

    case Date.compare(last_date, actual_date) do
      :lt ->
        get_bitcoin_price_list_slice(bitcon_price_list, last_date, actual_date)
        |> create_missing_bitcoin_list(last_date, sum_amount)

      _ ->
        []
    end
  end

  def from_index(bitcon_price_list, date) do
    case Enum.find_index(bitcon_price_list, fn x -> Date.compare(x.date, date) == :eq end) do
      nil ->
        length(bitcon_price_list)

      index ->
        case index + 1 >= 0 do
          true -> index + 1
          false -> index
        end
    end
  end

  def amount(bitcon_price_list, from_index, date) do
    case Enum.find_index(bitcon_price_list, fn x -> Date.compare(x.date, date) == :eq end) do
      nil -> length(bitcon_price_list) - from_index
      index -> index - from_index
    end
  end

  defp value(bitcoin_price, amount) do
    Float.round(bitcoin_price * amount, 2)
  end

  def to_date(datetime) do
    Timex.to_date(datetime)
  end

  defp now() do
    to_date(Timex.now())
  end

  def yesterday() do
    to_date(Timex.subtract(Timex.today(), Timex.Duration.from_days(1)))
  end
end
