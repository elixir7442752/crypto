defmodule Backend.FileType do
  @moduledoc false
  defstruct kind: nil, filename: nil

  def etoro, do: :etoro
  def pocket, do: :pocket
  def crypto_transactions, do: :crypto_transactions
  def krypto_transactions, do: :krypto_transactions
end
