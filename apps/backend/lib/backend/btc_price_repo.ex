defmodule Backend.BtcPriceRepo do
  @moduledoc false
  import Ecto.Query, warn: false

  alias Backend.Data.{
    BitcoinPrice
  }

  alias Backend.Repo
  require Logger

  def create_bitcoin_price_changeset(params) do
    BitcoinPrice.changeset(%BitcoinPrice{}, params)
  end

  def load_existing_dates(from_date, to_date) do
    Repo.all(
      from(b in BitcoinPrice,
        where: b.date >= ^from_date and b.date <= ^to_date,
        order_by: [asc: :date],
        select: b.date
      )
    )
  end

  def load_existing_dates_and_amounts(from_date, to_date) do
    Repo.all(
      from(b in BitcoinPrice,
        where: b.date >= ^from_date and b.date <= ^to_date,
        order_by: [asc: :date],
        select: %{date: b.date, amount: b.amount}
      )
    )
  end

  def get_bitcoin_price_for_date(date) do
    Repo.one(from(b in BitcoinPrice, where: b.date == ^date))
  end

  def save(entity) do
    Repo.insert(entity)
  end
end
