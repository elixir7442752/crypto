defmodule Backend.Factory do
  alias Backend.Data.{
    VibanPurchase,
    CryptoPurchase,
    RecurringBuyOrder,
    CryptoEarn,
    EtoroGbt,
    PocketPurchase
  }

  require Logger

  def create(%{:transaction_kind => "viban_purchase"} = params) do
    params = %{
      params
      | bitcoin_price_usd:
          calculate_bitcoin_price_in_usd(
            params.transaction_kind,
            params.to_currency,
            params.to_amount,
            params.native_amount_usd
          )
    }

    params = %{
      params
      | bitcoin_price_euro:
          calculate_bitcoin_price_in_euro(
            params.transaction_kind,
            params.to_currency,
            params.to_amount,
            params.native_amount
          )
    }

    VibanPurchase.changeset(%VibanPurchase{}, params)
  end

  def create(%{:transaction_kind => "recurring_buy_order"} = params) do
    params = %{
      params
      | bitcoin_price_usd:
          calculate_bitcoin_price_in_usd(
            params.transaction_kind,
            params.to_currency,
            params.to_amount,
            params.native_amount_usd
          )
    }

    params = %{
      params
      | bitcoin_price_euro:
          calculate_bitcoin_price_in_euro(
            params.transaction_kind,
            params.to_currency,
            params.to_amount,
            params.native_amount
          )
    }

    RecurringBuyOrder.changeset(%RecurringBuyOrder{}, params)
  end

  def create(%{:transaction_kind => "crypto_purchase"} = params) do
    params = %{
      params
      | bitcoin_price_usd:
          calculate_bitcoin_price_in_usd(
            params.transaction_kind,
            params.currency,
            params.amount,
            params.native_amount_usd
          )
    }

    params = %{
      params
      | bitcoin_price_euro:
          calculate_bitcoin_price_in_euro(
            params.transaction_kind,
            params.currency,
            params.amount,
            params.native_amount
          )
    }

    params = %{params | to_amount: params.amount}
    CryptoPurchase.changeset(%CryptoPurchase{}, params)
  end

  def create(%{:transaction_kind => "crypto_earn_interest_paid"} = params) do
    params = %{
      params
      | bitcoin_price_usd:
          calculate_bitcoin_price_in_usd(
            params.transaction_kind,
            params.currency,
            params.amount,
            params.native_amount_usd
          )
    }

    params = %{
      params
      | bitcoin_price_euro:
          calculate_bitcoin_price_in_euro(
            params.transaction_kind,
            params.currency,
            params.amount,
            params.native_amount
          )
    }

    params = %{params | to_amount: params.amount}
    CryptoEarn.changeset(%CryptoEarn{}, params)
  end


  def create(%{:transaction_kind => "etoro_gbt"} = params) do
    EtoroGbt.changeset(%EtoroGbt{}, params)
  end

  def create(%{:transaction_kind => "pocket", :transaction_description => "exchange"} = params) do
    PocketPurchase.changeset(%PocketPurchase{}, params)
  end

  def create(%{:transaction_kind => "pocket"} = _params) do
    []
  end

  def create(_params) do
    []
  end

  def calculate_bitcoin_price_in_usd("crypto_purchase", "BTC", curreny, native_amount_usd) do
    native_amount_usd / curreny
  end

  def calculate_bitcoin_price_in_usd(_transaction_kind, "BTC", to_amount, native_amount_usd) do
    native_amount_usd / to_amount
  end

  def calculate_bitcoin_price_in_usd(_transaction_kind, _other, _to_amount, _amount) do
    0.0
  end

  def calculate_bitcoin_price_in_euro("crypto_purchase", "BTC", currency, native_amount) do
    native_amount / currency
  end

  def calculate_bitcoin_price_in_euro(_transaction_kind, "BTC", to_amount, native_amount) do
    native_amount / to_amount
  end

  def calculate_bitcoin_price_in_euro(_transaction_kind, _other, _to_amount, _native_amount) do
    0.0
  end

end
