defmodule Backend.Data.PocketPurchase do
  use Ecto.Schema
  use LiveAdmin.Resource, create_with: false
  import Ecto.Changeset
  require Logger

  schema "pocket_purchases" do
    field(:timestamp_utc, :utc_datetime)
    field(:transaction_description, :string)
    field(:currency, :string)
    field(:amount, :float)
    field(:to_currency, :string)
    field(:to_amount, :float)
    field(:native_currency, :string)
    field(:native_amount, :float)
    field(:native_amount_usd, :float)
    field(:bitcoin_price_usd, :float)
    field(:bitcoin_price_euro, :float)
    field(:fee_currency, :string)
    field(:fee_amount, :float)
    belongs_to(:user, Backend.Accounts.User)

    timestamps()
  end

  def changeset(pocket_purchase, params) do
    pocket_purchase
    |> cast(params, [
      :user_id,
      :timestamp_utc,
      :transaction_description,
      :currency,
      :amount,
      :to_currency,
      :to_amount,
      :native_currency,
      :native_amount,
      :native_amount_usd,
      :bitcoin_price_usd,
      :bitcoin_price_euro,
      :fee_currency,
      :fee_amount
    ])
    |> unique_constraint([:timestamp_utc, :user_id])
  end
end
