defmodule Backend.Data.EtoroGbt do
  use Ecto.Schema
  use LiveAdmin.Resource, create_with: false
  import Ecto.Changeset
  require Logger

  schema "etoro_gbt" do
    field(:timestamp_utc, :utc_datetime)
    field(:transaction_description, :string)
    field(:currency, :string)
    field(:amount, :float)
    field(:to_currency, :string)
    field(:to_amount, :float)
    field(:native_currency, :string)
    field(:native_amount, :float)
    field(:native_amount_usd, :float)
    field(:spread, :float)
    field(:bitcoin_price_usd, :float)
    field(:bitcoin_price_euro, :float)
    belongs_to(:user, Backend.Accounts.User)

    timestamps()
  end

  def changeset(etoro_gbt, params) do
    etoro_gbt
    |> cast(params, [
      :user_id,
      :timestamp_utc,
      :transaction_description,
      :currency,
      :amount,
      :to_currency,
      :to_amount,
      :native_currency,
      :native_amount,
      :native_amount_usd,
      :spread,
      :bitcoin_price_usd,
      :bitcoin_price_euro
    ])
    |> unique_constraint([:timestamp_utc, :user_id])
  end
end
