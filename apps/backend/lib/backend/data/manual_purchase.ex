defmodule Backend.Data.ManualPurchase do
  use Ecto.Schema
  use LiveAdmin.Resource, create_with: false
  import Ecto.Changeset

  schema "manual_purchases" do
    field(:timestamp_utc, :utc_datetime)
    field(:transaction_description, :string)
    field(:currency, :string)
    field(:amount, :float)
    field(:to_currency, :string)
    field(:to_amount, :float)
    field(:native_currency, :string)
    field(:native_amount, :float)
    field(:native_amount_usd, :float)
    field(:bitcoin_price_usd, :float)
    field(:bitcoin_price_euro, :float)
    field(:fee_currency, :string)
    field(:fee_amount, :float)
    belongs_to(:user, Backend.Accounts.User)
    timestamps()
  end

  @doc false
  def changeset(manual_purchase, params) do
    manual_purchase
    |> cast(params, [
      :user_id,
      :timestamp_utc,
      :transaction_description,
      :currency,
      :amount,
      :to_currency,
      :to_amount,
      :native_currency,
      :native_amount,
      :native_amount_usd,
      :bitcoin_price_usd,
      :bitcoin_price_euro,
      :fee_currency,
      :fee_amount
    ])
    |> unique_constraint([:timestamp_utc, :user_id])
    |> validate_required([
      :user_id,
      :timestamp_utc,
      :currency,
      :amount,
      :to_amount,
      :native_amount,
      :native_amount_usd,
      :bitcoin_price_usd,
      :bitcoin_price_euro
    ])
  end
end
