defmodule Backend.Data.BitcoinPrice do
  use Ecto.Schema
  use LiveAdmin.Resource, create_with: false
  import Ecto.Changeset

  schema "bitcoin_prices" do
    field(:date, :date)
    field(:amount, :float)
    field(:base, :string)
    field(:currency, :string)

    timestamps()
  end

  def changeset(bitcoin_price, params) do
    bitcoin_price
    |> cast(params, [
      :date,
      :amount,
      :base,
      :currency
    ])
    |> unique_constraint([:date])
  end
end
