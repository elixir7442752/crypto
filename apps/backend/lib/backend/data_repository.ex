defmodule Backend.DataRepository do
  @moduledoc false
  import Ecto.Query, warn: false

  alias Backend.Repo
  require Logger

  alias Backend.Data.{
    VibanPurchase,
    CryptoPurchase,
    RecurringBuyOrder,
    CryptoEarn,
    EtoroGbt,
    PocketPurchase,
    ManualPurchase
  }

  def get_recurring_buy_orders(user_id, to_currency) do
    Repo.all(
      from(r in RecurringBuyOrder,
        where: r.user_id == ^user_id and r.to_currency == ^to_currency,
        order_by: [desc: :timestamp_utc]
      )
    )
  end

  def get_crypto_earns(user_id, currency) do
    Repo.all(
      from(r in CryptoEarn,
        where: r.user_id == ^user_id and r.currency == ^currency,
        order_by: [desc: :timestamp_utc]
      )
    )
  end

  def get_viban_purchases(user_id, to_currency) do
    Repo.all(
      from(v in VibanPurchase,
        where: v.user_id == ^user_id and v.to_currency == ^to_currency,
        order_by: [desc: :timestamp_utc]
      )
    )
  end

  def get_crypto_purchases(user_id, currency) do
    Repo.all(
      from(c in CryptoPurchase,
        where: c.user_id == ^user_id and c.currency == ^currency,
        order_by: [desc: :timestamp_utc]
      )
    )
  end

  def get_etoro_gbt(user_id, currency) do
    Repo.all(
      from(c in EtoroGbt,
        where: c.user_id == ^user_id and c.to_currency == ^currency,
        order_by: [desc: :timestamp_utc]
      )
    )
  end

  def get_pocket_purchases(user_id, currency) do
    Repo.all(
      from(c in PocketPurchase,
        where: c.user_id == ^user_id and c.to_currency == ^currency,
        order_by: [desc: :timestamp_utc]
      )
    )
  end

  def get_manual_purchases(user_id, currency) do
    Repo.all(
      from(c in ManualPurchase,
        where: c.user_id == ^user_id and c.to_currency == ^currency,
        order_by: [desc: :timestamp_utc]
      )
    )
  end

  def get_manual_purchase(id) do
    Repo.get(ManualPurchase, id)
  end

  def get_rbo_sum(user_id, currency) do
    Repo.one(
      from(r in RecurringBuyOrder,
        where: r.user_id == ^user_id and r.to_currency == ^currency,
        select: [sum(r.amount), sum(r.native_amount_usd), sum(r.to_amount)]
      )
    )
  end

  def get_vp_sum(user_id, currency) do
    Repo.one(
      from(v in VibanPurchase,
        where: v.user_id == ^user_id and v.to_currency == ^currency,
        select: [sum(v.native_amount), sum(v.native_amount_usd), sum(v.to_amount)]
      )
    )
  end

  def get_cp_sum(user_id, currency) do
    Repo.one(
      from(v in CryptoPurchase,
        where: v.user_id == ^user_id and v.currency == ^currency,
        select: [sum(v.native_amount), sum(v.native_amount_usd), sum(v.amount)]
      )
    )
  end

  def save(entity) do
    case Repo.insert(entity) do
      {:ok, entity} ->
        Logger.info("saved entity successful")
        {:ok, entity}

      {:error, entity} ->
        # Logger.error("error while saving entity: #{inspect(entity)}}")
        {:error, entity}
    end
  end

  def change_manual_purchase(%ManualPurchase{} = manual_purchase, attrs \\ %{}) do
    ManualPurchase.changeset(manual_purchase, attrs)
  end

  def create_manual_purchase(attrs \\ %{}) do
    %ManualPurchase{}
    |> ManualPurchase.changeset(attrs)
    |> Repo.insert()
  end

  def update_manual_purchase(%ManualPurchase{} = manual_purchase, attrs) do
    manual_purchase
    |> ManualPurchase.changeset(attrs)
    |> Repo.update()
  end
end
