defmodule Backend.Clients.Coinbase do
  use GenServer
  alias Backend.Clients.BtcPrice
  @moduledoc false

  require Logger

  def trigger() do
    GenServer.cast(__MODULE__, :trigger_price)
  end

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  @impl true
  def init(_opts) do
    {:ok, %{}, {:continue, :init_price_call}}
  end

  @impl true
  def handle_info(:get_internal_price, state) do
    get_internal_price()
    {:noreply, state}
  end

  @impl true
  def handle_continue(:init_price_call, state) do
    get_internal_price()
    {:noreply, state}
  end

  @impl true
  def handle_call(_msg, _from, state) do
    {:reply, :ok, state}
  end

  @impl true
  def handle_cast(:trigger_price, state) do
    get_price()
    {:noreply, state}
  end

  @impl true
  def handle_cast(_msg, state) do
    {:noreply, state}
  end

  defp get_internal_price() do
    Process.send_after(self(), :get_internal_price, 1000 * 60 * 60 * 24)
    get_price()
  end

  def get_price() do
    Logger.info("starting importing of coinbase bitcoins prices")
    BtcPrice.import(2019, 2025)
  end
end
