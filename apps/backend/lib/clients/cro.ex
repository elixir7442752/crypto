defmodule Backend.Clients.Cro do
  use GenServer

  @priceurl "https://api.coingecko.com/api/v3/simple/price?ids=crypto-com-chain&vs_currencies=eur%2Cusd"
  @moduledoc false

  alias Backend.Clients.HttpClient
  require Logger

  def trigger() do
    GenServer.cast(__MODULE__, :trigger_price)
  end

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  @impl true
  def init(_opts) do
    state = %{euro: 0.0, usd: 0.0}
    {:ok, state, {:continue, :init_price_call}}
  end

  @impl true
  def handle_info(:get_internal_price, state) do
    {:ok, new_state} = get_internal_price()
    broadcast(new_state, state)
    {:noreply, new_state}
  end

  @impl true
  def handle_continue(:init_price_call, _state) do
    case get_internal_price() do
      {:ok, new_state} -> {:noreply, new_state}
      {:error, _reason} -> {:noreply, %{euro: 0.0, usd: 0.0}}
    end
  end

  @impl true
  def handle_call(_msg, _from, state) do
    {:reply, :ok, state}
  end

  @impl true
  def handle_cast(:trigger_price, state) do
    {:ok, new_state} = get_price()
    broadcast(new_state, state)
    {:noreply, new_state}
  end

  @impl true
  def handle_cast(_msg, state) do
    {:noreply, state}
  end

  defp broadcast(new_state, _state) do
    Phoenix.PubSub.broadcast(Backend.PubSub, "backend", {:cro_price, new_state})
  end

  defp get_internal_price() do
    Process.send_after(self(), :get_internal_price, 20_000)
    get_price()
  end

  def get_price() do
    HttpClient.get(@priceurl, 400)
  end
end
