defmodule Backend.Clients.Price.Supervisor do
  @moduledoc false

  use Supervisor

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(arg) do
    children = [
      %{
        id: Backend.Clients.Bitcoin,
        start: {Backend.Clients.Bitcoin, :start_link, [arg]}
      },
      %{
        id: Backend.Clients.UsdEuro,
        start: {Backend.Clients.UsdEuro, :start_link, [arg]}
      },
      %{
        id: Backend.Clients.Block,
        start: {Backend.Clients.Block, :start_link, [arg]}
      },
      %{
        id: Backend.Clients.Coinbase,
        start: {Backend.Clients.Coinbase, :start_link, [arg]}
      }
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
