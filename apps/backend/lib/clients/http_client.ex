defmodule Backend.Clients.HttpClient do
  @moduledoc false

  @http_options [{:timeout, 2000}, {:ssl, [{:verify, :verify_none}]}]

  def get(url) do
    get(url, 200)
  end

  def get(url, _timeout) when is_bitstring(url) do
    case request(url) do
      {:ok, {{~c"HTTP/1.1", 200, ~c"OK"}, _headers, body}} ->
        {:ok, map} = Jason.decode(body)
        {:ok, %{euro: get_euro(map), usd: get_usd(map)}}

      reason ->
        {:error, "#{inspect(reason)}"}
    end
  end

  def get_latest_block(url) when is_bitstring(url) do
    case request(url) do
      {:ok, {{~c"HTTP/1.1", 200, ~c"OK"}, _headers, body}} ->
        {:ok, map} = Jason.decode(body)
        {:ok, %{block: get_block(map), block_time: get_time(map), txs_count: 0}}

      reason ->
        {:error, "#{inspect(reason)}"}
    end
  end

  def get_btc_price_per_date(url) do
    case request(url) do
      {:ok, {{~c"HTTP/1.1", 200, ~c"OK"}, _headers, body}} ->
        {:ok, map} = Jason.decode(body)
        {:ok, %{amount: get_amount(map), base: "BTC", currency: "USD"}}

      reason ->
        {:error, "#{inspect(reason)}"}
    end
  end

  defp request(url) do
    :httpc.request(:get, {to_charlist(url), []}, @http_options, body_format: :binary)
  end

  defp get_block(%{"height" => block}) do
    block
  end

  def get_amount(%{"data" => %{"amount" => amount}}) do
    amount
  end

  defp get_time(%{"time" => time}) do
    Float.round(((DateTime.utc_now() |> DateTime.to_unix()) - time) / 60, 0) |> trunc
  end

  defp get_euro(%{"EUR" => %{"last" => last}}) do
    last
  end

  defp get_euro(%{"crypto-com-chain" => %{"eur" => euro}}) do
    euro
  end

  defp get_euro(%{"conversion_rates" => %{"EUR" => euro}}) do
    euro
  end

  defp get_usd(%{"conversion_rates" => %{"USD" => usd}}) do
    usd
  end

  defp get_usd(%{"USD" => %{"last" => last}}) do
    last
  end

  defp get_usd(%{"crypto-com-chain" => %{"usd" => usd}}) do
    usd
  end
end
