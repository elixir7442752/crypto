defmodule Backend.Clients.Block do
  use GenServer

  alias Backend.Clients.HttpClient
  require Logger

  @moduledoc false

  @blockurl "https://blockchain.info/latestblock"

  def trigger() do
    GenServer.cast(__MODULE__, :trigger_block)
  end

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  @impl true
  def init(_opts) do
    state = %{block: 0, block_time: 0, tx_count: 0}
    {:ok, state, {:continue, :init_block_call}}
  end

  @impl true
  def handle_call(_msg, _from, state) do
    {:reply, :ok, state}
  end

  @impl true
  def handle_cast(:trigger_block, state) do
    {:ok, new_state} = get_block()
    broadcast(new_state, state)
    {:noreply, new_state}
  end

  @impl true
  def handle_cast(_msg, state) do
    {:noreply, state}
  end

  @impl true
  def handle_continue(:init_block_call, _state) do
    case get_internal_block() do
      {:ok, new_state} -> {:noreply, new_state}
      {:error, _reason} -> {:noreply, {:block, %{block: 0, block_time: 0, tx_count: 0}}}
    end
  end

  @impl true
  def handle_info(:get_block, state) do
    case get_internal_block() do
      {:ok, new_state} ->
        broadcast(new_state, state)
        {:noreply, new_state}

      {:error, _} ->
        {:noreply, state}
    end
  end

  defp broadcast(new_state, _state) do
    Phoenix.PubSub.broadcast(Backend.PubSub, "backend", {:block, new_state})
  end

  defp get_internal_block() do
    Process.send_after(self(), :get_block, 5_000)
    get_block()
  end

  def get_block() do
    HttpClient.get_latest_block(@blockurl)
  end
end
