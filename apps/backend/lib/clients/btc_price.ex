defmodule Backend.Clients.BtcPrice do
  @moduledoc false
  alias Backend.BtcPriceRepo
  alias Backend.Clients.HttpClient

  require Logger

  def import(from_year, to_year) when from_year <= to_year do
    year(from_year, to_year)
    |> create_list_for_import
    |> import_btc_price()
  end

  def import_yesterday() do
    yesterday()
    |> import_one_day
  end

  def import_one_day(day) do
    case Timex.is_valid?(day) do
      true -> import_btc_price([day])
    end
  end

  def import_one_month(year, month) do
    days(year, month)
    |> create_list_for_import()
    |> import_btc_price
  end

  defp year(from_year, to_year) when from_year <= to_year do
    List.flatten(for year <- from_year..to_year, do: month(year))
  end

  defp month(year) do
    for month <- 1..12, do: days(year, month)
  end

  defp days(year, month) do
    days_per_month = Timex.days_in_month(year, month)

    for day <- 1..days_per_month,
        Date.compare(to_date(year, month, day), yesterday()) == :lt,
        do: to_date(year, month, day)
  end

  defp to_date(year, month, day) do
    {:ok, date} = Date.new(year, month, day)
    date
  end

  defp import_btc_price(list_of_dates) when is_list(list_of_dates) do
    Enum.each(list_of_dates, fn date -> save(get_btc_price(date)) end)
  end

  defp get_btc_price(date) do
    case HttpClient.get_btc_price_per_date(
           "https://api.coinbase.com/v2/prices/BTC-USD/spot?date=#{date}"
         ) do
      {:ok, result} ->
        {:ok, BtcPriceRepo.create_bitcoin_price_changeset(Map.put_new(result, :date, date))}

      {:error, reason} ->
        {:error, reason}
    end
  end

  defp save(bitcoin_price) do
    case bitcoin_price do
      {:error, reason} -> {:error, reason}
      {:ok, btc_price} -> BtcPriceRepo.save(btc_price)
    end
  end

  defp create_list_for_import(list_of_days) do
    first_day = List.first(list_of_days)
    last_day = List.last(list_of_days)
    db_list = BtcPriceRepo.load_existing_dates(first_day, last_day)
    diff(list_of_days, db_list)
  end

  defp diff(date_list, db_list) do
    MapSet.difference(MapSet.new(date_list), MapSet.new(db_list))
    |> MapSet.to_list()
  end

  defp yesterday() do
    Timex.subtract(Timex.today(), Timex.Duration.from_days(1))
  end
end
