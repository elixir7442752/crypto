# Backend

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `backend` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:backend, "~> 0.1.0"}
  ]
end

```

HOST=willy SECRET_KEY_BASE=vCjfhejBPZz5I7gt3swvBeuifUggFppjvYb2hiKSniYnNdeKg49JwbJfkvHheKRZ DATABASE_URL=ecto://postgres:postgres@localhost/backend_prod crypto/bin/crypto eval Backend.Release.migrate

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/backend](https://hexdocs.pm/backend).

## Kind of transactions
| Transaction type  | Meaning  |
|-------------------|----------|
| viban_purchase| Bitcoin Kauf |
| recurring_buy_order | Sparplan |

