# Since configuration is shared in umbrella projects, this file
# should only configure the :canteen_backend application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
import Config

config :backend,
  ecto_repos: [Backend.Repo],
  pubsub_server: Backend.PubSub

import_config "#{config_env()}.exs"
