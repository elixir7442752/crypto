# Since configuration is shared in umbrella projects, this file
# should only configure the :backend application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
import Config
# Configure your database
config :backend, Backend.Repo,
  show_sensitive_data_on_connection_error: true,
  username: "postgres",
  password: "postgres",
  database: "backend_test",
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox

config :backend, Backend.Accounts.Mailer, adapter: Bamboo.TestAdapter
