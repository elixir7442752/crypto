# Since configuration is shared in umbrella projects, this file
# should only configure the :backend application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
import Config

# Configure your database
config :backend, Backend.Repo,
  url: "ecto://postgres:postgres@localhost/backend_dev",
  port: 5432

config :backend, Backend.Accounts.Mailer,
  adapter: Bamboo.SendGridAdapter,
  api_key: {:system, "SENDGRID_API_KEY"},
  hackney_opts: [
    recv_timeout: :timer.minutes(1)
  ]
