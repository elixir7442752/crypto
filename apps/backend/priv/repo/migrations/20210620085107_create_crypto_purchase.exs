defmodule Backend.Repo.Migrations.CreateCryptoPurchase do
  use Ecto.Migration

  def change do
    create table(:crypto_purchases) do
      add :timestamp_utc, :utc_datetime
      add :transaction_description, :string
      add :currency, :string
      add :amount, :float
      add :to_currency, :string
      add :to_amount, :float
      add :native_currency, :string
      add :native_amount, :float
      add :native_amount_usd, :float
      add :bitcoin_price_usd, :float
      add :bitcoin_price_euro, :float

      timestamps()
    end

    create unique_index(:crypto_purchases, [:timestamp_utc])
  end
end
