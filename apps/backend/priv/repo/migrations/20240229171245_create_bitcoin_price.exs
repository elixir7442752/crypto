defmodule Backend.Repo.Migrations.CreateBitcoinPrice do
  use Ecto.Migration

  def change do
    create table(:bitcoin_prices) do
      add :date, :date
      add :amount, :float
      add :base, :string
      add :currency, :string

      timestamps()
    end
    create unique_index(:bitcoin_prices, [:date])
  end

end
