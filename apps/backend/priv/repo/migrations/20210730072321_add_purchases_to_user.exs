defmodule Backend.Repo.Migrations.AddPurchasesToUser do
  use Ecto.Migration

  def change do
    alter table(:card_cashbacks) do
      add :user_id, references(:users)
    end
    drop index(:card_cashbacks, [:timestamp_utc])
    create unique_index(:card_cashbacks, [:timestamp_utc, :user_id])

    alter table(:crypto_earns) do
      add :user_id, references(:users)
    end
    drop index(:crypto_earns, [:timestamp_utc])
    create unique_index(:crypto_earns, [:timestamp_utc, :user_id])

    alter table(:crypto_purchases) do
      add :user_id, references(:users)
    end
    drop index(:crypto_purchases, [:timestamp_utc])
    create unique_index(:crypto_purchases, [:timestamp_utc, :user_id])

    alter table(:viban_purchases) do
      add :user_id, references(:users)
    end
    drop index(:viban_purchases, [:timestamp_utc])
    create unique_index(:viban_purchases, [:timestamp_utc, :user_id])

    alter table(:recurring_buy_orders) do
      add :user_id, references(:users)
    end
    drop index(:recurring_buy_orders, [:timestamp_utc])
    create unique_index(:recurring_buy_orders, [:timestamp_utc, :user_id])
  end
end
