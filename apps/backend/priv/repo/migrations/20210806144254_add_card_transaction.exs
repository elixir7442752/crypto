defmodule Backend.Repo.Migrations.AddCardTransaction do
  use Ecto.Migration

  def change do
    create table(:card_transactions) do
      add :timestamp_utc, :utc_datetime
      add :transaction_description, :string
      add :currency, :string
      add :amount, :float
      add :to_currency, :string
      add :to_amount, :float
      add :native_currency, :string
      add :native_amount, :float
      add :native_amount_usd, :float
      add :user_id, references(:users)

      timestamps()
    end
    create unique_index(:card_transactions, [:timestamp_utc, :user_id])
  end
end
