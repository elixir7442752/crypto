defmodule Backend.Repo.Migrations.CreateCardCashback do
  use Ecto.Migration

  def change do
    create table(:card_cashbacks) do
      add :timestamp_utc, :utc_datetime
      add :transaction_description, :string
      add :currency, :string
      add :amount, :float
      add :to_currency, :string
      add :to_amount, :float
      add :native_currency, :string
      add :native_amount, :float
      add :native_amount_usd, :float
      add :cro_price_usd, :float
      add :cro_price_euro, :float

      timestamps()
    end
    create unique_index(:card_cashbacks, [:timestamp_utc])
  end
end
