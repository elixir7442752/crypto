defmodule Backend.Repo.Migrations.CreatePocketPuchase do
  use Ecto.Migration

  def change do
    create table(:pocket_purchases) do
      add :timestamp_utc, :utc_datetime
      add :transaction_description, :string
      add :currency, :string
      add :amount, :float
      add :to_currency, :string
      add :to_amount, :float
      add :native_currency, :string
      add :native_amount, :float
      add :native_amount_usd, :float
      add :bitcoin_price_usd, :float
      add :bitcoin_price_euro, :float
      add :fee_currency, :string
      add :fee_amount, :float
      add :user_id, references(:users)
      timestamps()
    end

    create unique_index(:pocket_purchases, [:timestamp_utc, :user_id])
  end
end
