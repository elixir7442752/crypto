defmodule Backend.Repo.Migrations.CreateStakeInterest do
  use Ecto.Migration

  def change do
    create table(:stake_interest) do
      add :timestamp_utc, :utc_datetime
      add :staked_currency, :string
      add :staked_amount, :float
      add :apr, :string
      add :interest_currency, :string
      add :interest_amount, :float
      add :status, :string
      add :cro_price_usd, :float
      add :cro_price_euro, :float
      add :user_id, references(:users)

      timestamps()
    end
    create unique_index(:stake_interest, [:timestamp_utc, :user_id])
  end
end
