defmodule Backend.Repo.Migrations.CreateEtoroGbt do
  use Ecto.Migration

  def change do
    create table(:etoro_gbt) do
      add :timestamp_utc, :utc_datetime
      add :transaction_description, :string
      add :currency, :string
      add :amount, :float
      add :to_currency, :string
      add :to_amount, :float
      add :native_currency, :string
      add :native_amount, :float
      add :native_amount_usd, :float
      add :spread, :float
      add :bitcoin_price_usd, :float
      add :bitcoin_price_euro, :float
      add :user_id, references(:users)

      timestamps()
    end
    create unique_index(:etoro_gbt, [:timestamp_utc, :user_id])

  end
end
