defmodule BtcGraphDataTest do
  use ExUnit.Case
  @moduledoc false

  alias Backend.Data.PocketPurchase

  require Logger

  test "get_bitcoin_price_list_slice" do
    result =
      BtcGraphData.get_bitcoin_price_list_slice(
        bitcoin_price_list_factory(),
        ~D[2023-01-03],
        ~D[2023-01-13]
      )

    assert result == [
             %{amount: 16668.95, date: ~D[2023-01-04]},
             %{amount: 16844.4, date: ~D[2023-01-05]},
             %{amount: 16944.4, date: ~D[2023-01-06]},
             %{date: ~D[2023-01-07], amount: 16944.4}
           ]
  end

  test "fill_if_missing_dates" do
    result =
      BtcGraphData.fill_if_missing_dates(
        bitcoin_price_list_factory(),
        1,
        ~D[2023-01-02],
        ~D[2023-01-07]
      )

    assert result == [
             %{date: ~D[2023-01-03], sum_amount: 1, value: 16666.86},
             %{value: 16668.95, date: ~D[2023-01-04], sum_amount: 1},
             %{value: 16844.4, date: ~D[2023-01-05], sum_amount: 1},
             %{value: 16944.4, date: ~D[2023-01-06], sum_amount: 1}
           ]
  end

  test "cumulative_sum_fill_one_between" do
    result =
      BtcGraphData.cumulative_sum(
        purchase_factory(),
        %{date: 0, value: 0.0, sum_amount: 0.0},
        12000.0,
        [],
        bitcoin_price_list_factory()
      )
      |> Enum.sort_by(& &1.date, {:asc, Date})

    assert result = [
             %{value: 10000.0, date: ~D[2023-01-02], sum_amount: 1},
             %{value: 16666.86, date: ~D[2023-01-03], sum_amount: 1},
             %{value: 60000.0, date: ~D[2023-01-04], sum_amount: 3},
             %{value: 50533.2, date: ~D[2023-01-05], sum_amount: 3},
             %{value: 36000.0, date: ~D[2024-03-17], sum_amount: 3}
           ]
  end

  def purchase_factory() do
    [
      %PocketPurchase{
        timestamp_utc: ~D[2023-01-02],
        bitcoin_price_usd: 10000.0,
        to_amount: 1
      },
      %PocketPurchase{
        timestamp_utc: ~D[2023-01-04],
        bitcoin_price_usd: 20000.0,
        to_amount: 2
      }
    ]
  end

  def bitcoin_price_list_factory() do
    [
      %{date: ~D[2023-01-02], amount: 16611.9},
      %{date: ~D[2023-01-03], amount: 16666.86},
      %{date: ~D[2023-01-04], amount: 16668.95},
      %{date: ~D[2023-01-05], amount: 16844.4},
      %{date: ~D[2023-01-06], amount: 16944.4},
      %{date: ~D[2023-01-07], amount: 16944.4}
    ]
  end
end
