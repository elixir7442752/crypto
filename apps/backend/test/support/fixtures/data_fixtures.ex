defmodule Backend.DataFixtures do
  import Backend.AccountsFixtures

  @moduledoc """
  This module defines test helpers for creating
  entities via the `Backend.DataRepository` context.
  """

  @doc """
  Generate a manual_purchase.
  """
  def manual_purchase_fixture(attrs \\ %{}) do
    {:ok, manual_purchase} =
      attrs
      |> Enum.into(%{
        user_id: 1,
        bitcoin_price_euro: 40000.0,
        bitcoin_price_usd: 42000.0,
        native_amount: 40000.0,
        native_amount_usd: 42000.0,
        amount: 1,
        to_amount: 1,
        currency: "EUR",
        native_currency: "EUR",
        timestamp_utc: ~U[2023-12-14 08:42:45.280017Z]
      })
      |> Backend.DataRepository.create_manual_purchase()

    manual_purchase
  end
end
